from pyimport import pyimport
import urllib.request
import subprocess
import sys

# Cant stand OOP, but the syntax becomes nice in this case
tqdm = pyimport("tqdm")

class Download():     
    def __init__(self, url):
        self.tqdm = tqdm.tqdm
        self.url = url
        self.ready = False
        self.fname = "Download"
    
    def update(self, b=1, bsize=1, tsize=None):
        if tsize is not None and not self.ready:
            self.bar = self.tqdm(
                total = tsize, unit='B',
                unit_scale=True, miniters=1,
                desc=self.filename)
            self.ready = True
        
    def to (self, path):
        if path is not pathlib.Path:
            path = pathlib.Path(path)
        self.filename = path.stem
        path.parent.mkdir(parents=True, exist_ok=True)
        urllib.request.urlretreive(
            self.url, filename=path,
            reporthook=self.update)
        

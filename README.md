# Python Downloader
This is a small module with a cli progressbar making simple downloads easy. \
`python3 -m pip install git+https://gitlab.com/ekroll/pydl.git`


```python
# Example
from pydl import Download
Download(url).to(path)
```
